package rsm;


import Service.KeyGenerationService;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class KeyGenerationServiceTest {

    @Test
    public void generateKeysTest(){
        KeyGenerationService kgs = new KeyGenerationService();
        int keyNumber = 5000;
        String[] keys = kgs.requestKeys(keyNumber);

        //Test number of returned keys
        assertEquals(keyNumber, keys.length);

        //Test that each key is unique
        HashSet<String> keyhash = new HashSet<String>(Arrays.asList(keys));
        System.out.println(keyhash.size());
        assertEquals(keyNumber, keyhash.size());
    }
}
