package Service;

import java.util.*;

public class ShortURLService {

    Map<String, String> urls;

    List<String> availableUrl;

    KeyGenerationService kgs;

    private static ShortURLService instance = null;

    public ShortURLService(){
        kgs = new KeyGenerationService();
        availableUrl = new ArrayList<>();
        availableUrl.addAll(Arrays.asList(kgs.requestKeys(5)));
        urls = new HashMap<>();
    }

    public static ShortURLService getInstance(){
        if(instance == null){
            instance = new ShortURLService();
        }
        return instance;
    }

    public String createURL(String originalUrl, String customAlias, String userName){
        String sUrl;
        if(customAlias == null){
            sUrl = availableUrl.remove(0);
        }else if(urls.containsKey(customAlias)){
                sUrl = availableUrl.remove(0) + customAlias;

        }else{
            sUrl = customAlias;
        }
        if(availableUrl.size() == 0){
            availableUrl.addAll(Arrays.asList(kgs.requestKeys(5)));
        }
        urls.put(sUrl, originalUrl);
        return sUrl;
    }

    public String translateUrl(String shortUrl){
        if(urls.containsKey(shortUrl)) {
            return urls.get(shortUrl);
        }
        return "";
    }



}
