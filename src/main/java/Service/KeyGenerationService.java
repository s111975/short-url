package Service;

import java.util.Random;

public class KeyGenerationService {

    int keyLength = 6;
    Random random;

    public KeyGenerationService(){
        random = new Random();
    }
    public String[] requestKeys(int keyNumber) {
        String[] keys = new String[keyNumber];

        for(int i = 0; i < keyNumber; i++) {
            String key = random.ints(97, 122 + 1)
                    .limit(keyLength)
                    .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                    .toString();
            keys[i] = key;
        }

        return keys;
    }
}
