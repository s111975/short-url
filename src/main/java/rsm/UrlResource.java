package rsm;

import DTO.UrlDTO;
import Service.ShortURLService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;

@Path("/")
public class UrlResource {
    ShortURLService urlService = ShortURLService.getInstance();

    @Path("/url")
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String hello() {
        return "Hello RESTEasy!!";
    }

    @Path("/url")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response shortener(UrlDTO url){
        System.out.println("url: " +url.getOriginalUrl() + " " + url.getCustomAlias() + " " + url.getUserName());
        try {
            String shortUrl = urlService.createURL(url.getOriginalUrl(), url.getCustomAlias(), url.getUserName());
            return Response.created(URI.create(shortUrl)).build();
        }catch(Exception e){
            e.printStackTrace();
            return Response.serverError().build();
        }

    }

    @Path("{url}")
    @GET
    public Response getUrl(@PathParam("url") String url){
        String longUrl = urlService.translateUrl(url);
        if(longUrl.equals("")){
            return Response.status(404).entity("Url not found").build();
        }
        return Response.status(302).location(URI.create(longUrl)).build();

    }



}