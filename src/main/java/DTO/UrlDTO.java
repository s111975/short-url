package DTO;

//import javax.xml.bind.annotation.XmlRootElement;

//@XmlRootElement
public class UrlDTO {



    public String originalUrl;
    public String customAlias;
    public String userName;

    public UrlDTO(){}

    //public UrlDTO(String originalUrl){
        //this.originalUrl = originalUrl;
    //}

    public String getOriginalUrl() {
        return originalUrl;
    }

    public String getCustomAlias() {
        return customAlias;
    }

    public String getUserName() {
        return userName;
    }

    public void setOriginalUrl(String originalUrl) {
        this.originalUrl = originalUrl;
    }

    public void setCustomAlias(String customAlias) {
        this.customAlias = customAlias;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }



}
